package delatega.dz.quiz_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button button_true;
    private Button button_false;
    private TextView mQuestionTextView;
    private TextView textView_score;
    private ProgressBar progressBar_score;
    private int mIndexQuestion = 0;
    private int idCurrentQuestion;
    private int score=0;

        private TrueFalse[] mQuestionBank = new TrueFalse[] {
            new TrueFalse(R.string.question_1, true),
            new TrueFalse(R.string.question_2, true),
            new TrueFalse(R.string.question_3, true),
            new TrueFalse(R.string.question_4, true),
            new TrueFalse(R.string.question_5, true),
            new TrueFalse(R.string.question_6, false),
            new TrueFalse(R.string.question_7, true),
            new TrueFalse(R.string.question_8, false),
            new TrueFalse(R.string.question_9, true),
            new TrueFalse(R.string.question_10, true),
            new TrueFalse(R.string.question_11, false),
            new TrueFalse(R.string.question_12, false),
            new TrueFalse(R.string.question_13,true)
    };

    final int Progress_bar_increment = (int) Math.ceil(100.0/mQuestionBank.length);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            score = savedInstanceState.getInt("ScorKey");
            mIndexQuestion = savedInstanceState.getInt("IndexKey");
        }else{
            score=0;
            mIndexQuestion=0;
        }


    button_true = (Button) findViewById(R.id.button_vrai);
    button_false = (Button) findViewById(R.id.button_faux);
    mQuestionTextView = (TextView) findViewById(R.id.textView_question);
    textView_score = (TextView) findViewById(R.id.textView_score);
    textView_score.setText("Score : "+score+" / "+ mQuestionBank.length);

        progressBar_score = (ProgressBar) findViewById(R.id.progress_bar);
    idCurrentQuestion = mQuestionBank[mIndexQuestion].getIdQuestion();

    mQuestionTextView.setText(idCurrentQuestion);

        button_true.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check(true);
                updateIndex();
            }
        });
        button_false.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check(false);
                updateIndex();            }
        });

      //  VraiFaux question = new VraiFaux(R.string.question_1 , true);
    }
    private void updateIndex(){

        mIndexQuestion = (mIndexQuestion +1) % mQuestionBank.length;

        if (mIndexQuestion == 0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Game over");
            alert.setCancelable(false);
            alert.setMessage("your score is "+ score + " points");
            alert.setPositiveButton("close application", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.show();
        }

        idCurrentQuestion = mQuestionBank[mIndexQuestion].getIdQuestion();
        mQuestionTextView.setText(idCurrentQuestion);
        progressBar_score.incrementProgressBy(Progress_bar_increment);
        textView_score.setText("Score : "+score+" / "+ mQuestionBank.length);
    }
    private void check(boolean userAnswer){
        boolean correctAnswer = mQuestionBank[mIndexQuestion].isAnsarQuestion();
        if(userAnswer == correctAnswer){
            Toast.makeText(getApplicationContext(),R.string.correct_toast , Toast.LENGTH_SHORT);
            score++;
        }else{
            Toast.makeText(getApplicationContext(),R.string.incorrect_toast , Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("ScorKey" , score);
        outState.putInt("IndexKey" , mIndexQuestion);

    }
}