package delatega.dz.quiz_app;

public class TrueFalse {

    private int idQuestion;
    private boolean ansarQuestion;

    public TrueFalse(int idQuestion , boolean anserQuestion){
        this.idQuestion = idQuestion;
        this.ansarQuestion = anserQuestion;
    }

    public boolean isAnsarQuestion() {
        return ansarQuestion;
    }

    public void setAnsarQuestion(boolean ansarQuestion) {
        this.ansarQuestion = ansarQuestion;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }
}
